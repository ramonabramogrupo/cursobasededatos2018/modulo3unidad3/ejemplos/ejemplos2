$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"servidor mio","icon":"images/folder.svg","href":"Servers\\servidor_mio\\servidor_mio.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"ejemplo2programacion","icon":"images/database.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\ejemplo2programacion.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"datos","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Tables\\datos.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"e1a","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e1a.html","target":"DATA"},{"text":"e1b","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e1b.html","target":"DATA"},{"text":"e2","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e2.html","target":"DATA"},{"text":"e3","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e3.html","target":"DATA"},{"text":"e3a","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e3a.html","target":"DATA"},{"text":"e4","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e4.html","target":"DATA"},{"text":"e5","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e5.html","target":"DATA"},{"text":"e6","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e6.html","target":"DATA"},{"text":"e7","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e7.html","target":"DATA"},{"text":"e8","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplo2programacion\\Procedures\\e8.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}
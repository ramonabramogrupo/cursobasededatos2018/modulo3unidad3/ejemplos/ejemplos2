/**
  EJEMPLOS 2 DE PROGRAMACION
**/

  USE ejemplo2programacion;

  /* EJERCICIO 1
     Realizar un procedimiento almacenado que reciba un texto y un car�cter. 
     Debe indicarte si ese car�cter est� en el texto. 
     Deb�is realizarlo con: Locate, Position.
  */

  -- con locate
  DELIMITER //
  CREATE OR REPLACE PROCEDURE e1a (IN texto VARCHAR(200), caracter char(1))
    COMMENT 'Procedimiento almacenado que reciba un texto y un car�cter. 
    Debe indicarte si ese car�cter est� en el texto. Utilizar LOCATE'
  BEGIN
    DECLARE resultado varchar(30) DEFAULT "El caracter no esta";
    
    -- buscamos el caracter
    IF(LOCATE(caracter,texto)<>0) THEN
      SET resultado="El caracter esta";
    END IF;

    -- mostramos el resultado
    SELECT resultado;
  END //
  DELIMITER ;


  CALL e1a("ejemplo de clase","o");

-- con position
  DELIMITER //
  CREATE OR REPLACE PROCEDURE e1b (IN texto VARCHAR(200), caracter char(1))
    COMMENT 'Procedimiento almacenado que reciba un texto y un car�cter. 
    Debe indicarte si ese car�cter est� en el texto. Utilizar POSITION'
  BEGIN
    DECLARE resultado varchar(30) DEFAULT "El caracter no esta";
    
    -- buscamos el caracter
    IF(POSITION(caracter IN texto)<>0) THEN
      SET resultado="El caracter esta";
    END IF;

    -- mostramos el resultado
    SELECT resultado;
  END //
  DELIMITER ;

  CALL e1b("ejemplo de clase","o");

  /* EJERCICIO 2
      Realizar un procedimiento almacenado que reciba un texto y un car�cter. 
      Te debe indicar todo el texto que haya antes de la primera vez que aparece 
      ese car�cter.
  */

  DELIMITER //
  CREATE OR REPLACE PROCEDURE e2 (IN texto VARCHAR(200), caracter char(1))
    COMMENT 'Realizar un procedimiento almacenado que reciba un texto y un car�cter. 
      Te debe indicar todo el texto que haya antes de la primera vez que aparece 
      ese car�cter.'
  BEGIN
    DECLARE resultado varchar(30) DEFAULT NULL;
    
    -- buscamos el caracter y leemos el texto 
      SET resultado=SUBSTRING_INDEX(texto,caracter,1);

    -- mostramos el resultado
    SELECT resultado;
  END //
  DELIMITER ;

CALL e2("ejemplo de clase","o");

/* EJERCICIO 3
      Realizar un procedimiento almacenado que reciba: 
        - tres n�meros 
        - dos argumentos de tipo salida 
      Que devuelva el n�mero m�s grande y el n�mero m�s peque�o de los tres n�meros 
      pasados

    �Como utilizar el procediento?
    call e3(1,2,5,@grande,@pequeno);
    select @grande,@pequeno;
  */
DELIMITER //
CREATE OR REPLACE PROCEDURE e3
  (n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
COMMENT 'Realizar un procedimiento almacenado que reciba: 
        - tres n�meros 
        - dos argumentos de tipo salida 
      Que devuelva el n�mero m�s grande y el n�mero m�s peque�o de los tres n�meros 
      pasados'
BEGIN
  -- realizado con las funciones del lenguaje
  SET mayor=GREATEST(n1,n2,n3);
  SET menor=LEAST(n1,n2,n3);
END //
DELIMITER ;

CALL e3(10,2,54,@a,@b);
SELECT @a,@b;

-- realizar el ejemplo con if
DELIMITER //
CREATE OR REPLACE PROCEDURE e3a
  (n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
COMMENT 'Realizar un procedimiento almacenado que reciba: 
        - tres n�meros 
        - dos argumentos de tipo salida 
      Que devuelva el n�mero m�s grande y el n�mero m�s peque�o de los tres n�meros 
      pasados. Lo realizo con la funcion IF'
BEGIN
SET mayor=IF(n1>n2,IF(n1>n3,n1,n3),IF(n2>n3,n2,n3));
SET menor=IF(n1<n2,IF(n1<n3,n1,n3),IF(n2<n3,n2,n3));

END //
DELIMITER ;

CALL e3a(10,2,54,@a,@b);
SELECT @a,@b;

  /** 
    EJERCICIO 4
    Realizar un procedimiento almacenado que muestre cuantos numeros1 y numeros2 
    son mayores que 50

    CALL e4();

  **/

    DELIMITER //
    CREATE OR REPLACE PROCEDURE e4()
      COMMENT 'Realizar un procedimiento almacenado que muestre cuantos numeros1 y numeros2 
    son mayores que 50'
    BEGIN
      DECLARE resultado int; -- almacena resultado
      DECLARE n1 int; -- almacena el primer resultado
      DECLARE n2 int; -- almacena el segundo resultado
      
      -- calculos
      SELECT COUNT(*) INTO n1 FROM datos WHERE numero1>50; 
      SET n2=(SELECT COUNT(*) FROM datos WHERE numero2>50);
      SET resultado=n1+n2;
      
      -- salida
      SELECT resultado;
    END //
    DELIMITER ;

  CALL e4();

    /** 
    EJERCICIO 5
    Realizar un procedimiento almacenado que calcule la suma y la resta de 
    numero1 y numero2 de la tabla datos

    CALL e5();

  **/

    DELIMITER //
    CREATE OR REPLACE PROCEDURE e5()
      COMMENT 'Realizar un procedimiento almacenado que calcule la suma y la resta de 
    numero1 y numero2 de la tabla datos'
    BEGIN
      UPDATE datos d
        SET d.suma=d.numero1+d.numero2;
      UPDATE datos d
        SET d.resta=d.numero1-d.numero2;
    END //
    DELIMITER ;

    CALL e5();

    /** 
    EJERCICIO 6
    Realizar un procedimiento almacenado que primero ponga todos los valores 
    de suma y resta a NULL y depues calcule la suma 
    solamente si el numero1 es mayor que el numero2 y 
    calcule la resta de numero2-numero1 si el numero2 es mayor o 
    numero1-numero2 si es mayor el numero1

    CALL e6();
    select * from datos;

  **/

    DELIMITER //
    CREATE OR REPLACE PROCEDURE e6()
      COMMENT 'Realizar un procedimiento almacenado que primero ponga todos los valores 
    de suma y resta a NULL y depues calcule la suma 
    solamente si el numero1 es mayor que el numero2 y 
    calcule la resta de numero2-numero1 si el numero2 es mayor o 
    numero1-numero2 si es mayor el numero1'
    BEGIN
      -- inicializar los campos de la tabla
      UPDATE datos
        set suma=NULL, resta=NULL;

      
      -- opcion actualizando todos
      /*UPDATE datos d
        SET d.suma=IF(numero1>numero2,numero1+numero2,0);*/
      
      -- opcion actualizando solo los que cumplan la condicion
      UPDATE datos d
        SET d.suma=numero1+numero2
        WHERE numero1>numero2;

      -- utilizando la instruccion if
      /*IF (numero1>numero2) THEN 
          ??? necesito cursores
      END IF;*/
      
      -- calcular la resta

      -- realizando dos update
      UPDATE datos d  
        set d.resta=numero1-numero2
        WHERE numero1>numero2;

      UPDATE datos d  
        set d.resta=numero2-numero1
        WHERE numero1<=numero2;

      -- utilizando 1 solo update
      UPDATE datos
        SET resta=IF(numero1>numero2,numero1-numero2,numero2-numero1);
    
    END //
    DELIMITER ;

    CALL e6();
    SELECT * FROM datos;

  /** 
    Ejercicio 7
    Realizar un procedimiento almacenado que coloque en el campo 
    junto el texto1, texto2
    
    Para utilizarlo:
    call e7();
    select * from datos;
**/

  DELIMITER //
  CREATE OR REPLACE PROCEDURE e7 ()
    COMMENT 'Realizar un procedimiento almacenado que coloque en el campo 
    junto el texto1, texto2' 
  BEGIN
    UPDATE  datos 
      SET junto=CONCAT(texto1," ",texto2);
  END //
  DELIMITER ;

CALL e7();
SELECT * FROM datos;

/** 
    Ejercicio 8
  	Realizar un procedimiento almacenado que coloque en el campo junto 
    el valor NULL. 
    Despu�s debe colocar en el campo junto el texto1-texto2 si el rango es A 
    y si es rango B debe colocar texto1+texto2. 
    Si el rango es distinto debe colocar texto1 nada m�s
    
    Para utilizarlo:
    call e8();
    select * from datos;
**/

  DELIMITER //
  CREATE OR REPLACE PROCEDURE e8 ()
    COMMENT '	Realizar un procedimiento almacenado que coloque en el campo junto 
    el valor NULL. Despu�s debe colocar en el campo junto el texto1-texto2 si el rango es A 
    y si es rango B debe colocar texto1+texto2. Si el rango es distinto debe colocar texto1 nada m�s' 
  BEGIN
    -- inicializamos el campo
    UPDATE datos
      SET junto=NULL;

    -- quitar los espacios en blanco
    UPDATE datos d
      SET texto1=TRIM(texto1), texto2=TRIM(texto2);

    -- utilizamos la funcion if para no tener que realizar 3 update
    UPDATE  datos 
      SET junto=IF(rango='A',CONCAT(texto1,"-",texto2),IF(rango="B",CONCAT(texto1,"+",texto2),texto1));
  END //
  DELIMITER ;

CALL e8();
SELECT * FROM datos;